# Django-test

Just a test site where I can dump a bunch of random crap for testing and demo purposes.

## Running

This is only meant to be run locally. Maybe one of these days I will dockerize it, but for now:

```bash
# Clone the application
git clone https://gitlab.com/theepic-dev/gitlab-test
cd gitlab-test
# Create a virtualenv and install requirements
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
# Apply migrations
python manage.py migrate
# If this is the first time running the server, create a superuser
python manage.py createsuperuser
# Run the application
python manage.py runserver
```

The application should be available at http://localhost:8000/
